
CREATE TABLE requerimiento(
	id numeric(5,0) NOT NULL,
	requerimiento varchar(50) NOT NULL,
	descripcion varchar(200) NOT NULL,
	origen varchar(50) NOT NULL,
	estado varchar(10) NOT NULL,
	proyecto numeric(5,0) NOT NULL,
	CONSTRAINT pk_requerimiento PRIMARY KEY (id)

);

CREATE TABLE proyecto(
	id numeric(5,0) NOT NULL,
	nombre varchar(50) NOT NULL,
	descripcion varchar(200) NOT NULL,
	objetivo varchar(50) NOT NULL,
	fInicio date NOT NULL,
	fTermino date NOT NULL,
	estado varchar(10) NOT NULL,
	CONSTRAINT pk_proyecto PRIMARY KEY (id)

);

CREATE TABLE usuario(
	id numeric(5,0) NOT NULL,
	nombre varchar(50) NOT NULL,
	aPaterno varchar(50) NOT NULL,
	aMaterno varchar(50) NOT NULL,
	correo varchar(50) NOT NULL,
	password varchar(50) NOT NULL,
	estado varchar(10) NOT NULL,
	CONSTRAINT pk_usuario PRIMARY KEY (id)

);

CREATE TABLE actor(
	id numeric(5,0) NOT NULL,
	nombre varchar(50) NOT NULL,
	descripcion varchar(200) NOT NULL,
	CONSTRAINT pk_actor PRIMARY KEY (id)

);

CREATE TABLE historiaUsuario(
	id numeric(5,0) NOT NULL,
	titulo varchar(50) NOT NULL,
	descripcion varchar(200) NOT NULL,
	proyecto numeric(5,0) NOT NULL,
	CONSTRAINT pk_historia PRIMARY KEY (id)

);

CREATE TABLE criterioAceptacion(
	id numeric(5,0) NOT NULL,
	descripcion varchar(50) NOT NULL,
	historiaUsuario numeric(5,0) NOT NULL,
	CONSTRAINT pk_criterio PRIMARY KEY (id)

);

CREATE TABLE flujoAltExc(
	id numeric(5,0) NOT NULL,
	nombre varchar(50) NOT NULL,
	descripcion text NOT NULL,
	tipo varchar(10) NOT NULL,
	casoUso numeric(5,0) NOT NULL,
	CONSTRAINT pk_flujoAE PRIMARY KEY (id)

);

CREATE TABLE casoUso(
	id numeric(5,0) NOT NULL,
	titulo varchar(50) NOT NULL,
	descripcion varchar(200) NOT NULL,
	precondicion varchar(200) NOT NULL,
	fPrincipal text NOT NULL,
	postcondicion varchar(200) NOT NULL,
	estado varchar(10) NOT NULL,
	proyecto numeric(5,0) NOT NULL,
	CONSTRAINT pk_casoUso PRIMARY KEY (id)

);

CREATE TABLE nota(
	id numeric(5,0) NOT NULL,
	descripcion varchar(200) NOT NULL,
	flujo numeric(5,0) NOT NULL,
	CONSTRAINT pk_nota PRIMARY KEY (id)

);

CREATE TABLE rol(
	id numeric(5,0) NOT NULL,
	nombre varchar(50) NOT NULL,
	descripcion varchar(200) NOT NULL,
	CONSTRAINT pk_rol PRIMARY KEY (id)

);

CREATE TABLE reqXcaso(
	requerimiento numeric(5,0) NOT NULL,
	casoUso numeric(5,0) NOT NULL
);

CREATE TABLE usuarioXreq(
	usuario numeric(5,0) NOT NULL,
	requerimiento numeric(5,0) NOT NULL
);

CREATE TABLE usuarioXproyecto(
	usuario numeric(5,0) NOT NULL,
	proyecto numeric(5,0) NOT NULL,
	rol numeric(5,0) NOT NULL
);

CREATE TABLE requerimientoXhistoria(
	requerimiento numeric(5,0) NOT NULL,
	historia numeric(5,0) NOT NULL
);

CREATE TABLE casoUsoXactor(
	casoUso numeric(5,0) NOT NULL,
	actor numeric(5,0) NOT NULL
);

ALTER TABLE requerimiento ADD CONSTRAINT fk_req_proy FOREIGN KEY (proyecto)
REFERENCES proyecto (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE historiaUsuario ADD CONSTRAINT fk_proyecto_histUsu FOREIGN KEY (proyecto)
REFERENCES proyecto (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE criterioAceptacion ADD CONSTRAINT fk_historiaUsuario_criterio FOREIGN KEY (historiaUsuario)
REFERENCES historiaUsuario (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE flujoAltExc ADD CONSTRAINT fk_caso_flujo FOREIGN KEY (casoUso)
REFERENCES casoUso (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE casoUso ADD CONSTRAINT fk_casoUso_proyecto FOREIGN KEY (proyecto)
REFERENCES proyecto (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE nota ADD CONSTRAINT fk_casoUso_nota FOREIGN KEY (flujo)
REFERENCES casoUso (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE reqXcaso ADD CONSTRAINT fk_requerimientoXcaso_caso FOREIGN KEY (casoUso)
REFERENCES casoUso (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE reqXcaso ADD CONSTRAINT fk_requermientoXcaso_requerimiento FOREIGN KEY (requerimiento)
REFERENCES requerimiento (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE usuarioXreq ADD CONSTRAINT fk_usuario_requerimiento FOREIGN KEY (usuario)
REFERENCES usuario (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE usuarioXreq ADD CONSTRAINT fk_usuario_requerimiento_req FOREIGN KEY (requerimiento)
REFERENCES requerimiento (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE usuarioXproyecto ADD CONSTRAINT fk_usuario_proyecto FOREIGN KEY (usuario)
REFERENCES usuario (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE usuarioXproyecto ADD CONSTRAINT fk_usurio_proyecto_proyecto FOREIGN KEY (proyecto)
REFERENCES proyecto (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE usuarioXproyecto ADD CONSTRAINT fk_rol_usuarioXproyecto FOREIGN KEY (rol)
REFERENCES rol (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE requerimientoXhistoria ADD CONSTRAINT fk_req_hist_req FOREIGN KEY (requerimiento)
REFERENCES requerimiento (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE requerimientoXhistoria ADD CONSTRAINT fk_req_hist_hist FOREIGN KEY (historia)
REFERENCES historiaUsuario (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE casoUsoXactor ADD CONSTRAINT fk_casoUso_actor FOREIGN KEY (casoUso)
REFERENCES casoUso (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE actor ADD proyecto  numeric(5,0);
ALTER TABLE rol ADD proyecto  numeric(5,0) NULL;

ALTER TABLE actor ADD CONSTRAINT fk_actor_proyecto FOREIGN KEY (proyecto)
REFERENCES proyecto (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE rol ADD CONSTRAINT fk_rol_proyecto FOREIGN KEY (proyecto)
REFERENCES proyecto (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;


CREATE SEQUENCE requerimiento_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE requerimiento ALTER COLUMN id SET DEFAULT nextval('requerimiento_seq');

CREATE SEQUENCE proyecto_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE proyecto ALTER COLUMN id SET DEFAULT nextval('proyecto_seq');

CREATE SEQUENCE usuario_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE usuario ALTER COLUMN id SET DEFAULT nextval('usuario_seq');

CREATE SEQUENCE actor_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE actor ALTER COLUMN id SET DEFAULT nextval('actor_seq');

CREATE SEQUENCE historiaUsuario_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE historiaUsuario ALTER COLUMN id SET DEFAULT nextval('historiaUsuario_seq');

CREATE SEQUENCE criterioAceptacion_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE criterioAceptacion ALTER COLUMN id SET DEFAULT nextval('criterioAceptacion_seq');

CREATE SEQUENCE flujoAltExc_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE flujoAltExc ALTER COLUMN id SET DEFAULT nextval('flujoAltExc_seq');

CREATE SEQUENCE casoUso_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE casoUso ALTER COLUMN id SET DEFAULT nextval('casoUso_seq');

CREATE SEQUENCE nota_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE nota ALTER COLUMN id SET DEFAULT nextval('nota_seq');

CREATE SEQUENCE rol_seq
INCREMENT BY 1
MINVALUE 1
NO MAXVALUE 
START WITH 1
NO CYCLE;

ALTER TABLE rol ALTER COLUMN id SET DEFAULT nextval('rol_seq');

INSERT INTO rol (nombre, descripcion) 
VALUES ('creador','Es el que crea el proyecto');


INSERT INTO usuario(nombre, aPaterno, aMaterno, correo, password, estado)
VALUES ('root','root', 'root', 'root@correo.mx', 'hola123.', 'admin');

INSERT INTO usuario (nombre,apaterno,amaterno,correo,password,estado) VALUES('Alan','Gutierrez','Banuelos','alan@correo.com','hola123.','activo');

INSERT INTO usuario (nombre,apaterno,amaterno,correo,password,estado) VALUES('Alejandro','Magno','Aristoteles','magno@correo.com','hola123.','inactivo');

INSERT INTO proyecto(nombre, descripcion, objetivo, fInicio, fTermino, estado)
VALUES ('Proyecto de Prueba', 'Descripcion del proyecto', 'Objetivo del proyecto', '2001-10-05', '2001-10-05', 'activo');

INSERT INTO rol (nombre,descripcion,proyecto) VALUES ('desarrollador','genera código',1);
INSERT INTO rol (nombre,descripcion,proyecto) VALUES ('probador','verifica requerimientos',1);

INSERT INTO actor (nombre,descripcion,proyecto) VALUES ('Usuario','Utilizará el sistema',1);

INSERT INTO casoUso(titulo, descripcion, precondicion, fPrincipal, postcondicion, estado, proyecto)
VALUES ('Caso de Uso','Descripción caso de uso', 'Precondicion', 'Flujo principal', 'postcondicion', 'activo', 1);

INSERT INTO historiaUsuario(titulo, descripcion, proyecto)
VALUES ('HU de Prueba', 'Descripcion de HU', 1);

INSERT INTO requerimiento(requerimiento, descripcion, origen, estado, proyecto)
VALUES ('Requerimiento de Prueba', 'Descripcion del requerimiento', 'Juan T.', 'activo', 1);

INSERT INTO criterioAceptacion(descripcion, historiaUsuario)
VALUES ('Criterio de Aceptación', 1);

INSERT INTO nota(descripcion, flujo)
VALUES ('Nota del Caso de uso', 1);

INSERT INTO flujoAltExc(nombre, descripcion, tipo, casoUso)
VALUES ('Flujo del modulo x', 'Pasos del flujo', 'alterno', 1);

INSERT INTO flujoaltexc (nombre,descripcion,tipo,casouso) VALUES ('Flujo de excepcion 2','Se debe realizar el flujo 2','excepcion',1);
INSERT INTO flujoaltexc (nombre,descripcion,tipo,casouso) VALUES ('Flujo de excepcion 3','Se debe realizar el flujo 3','excepcion',1);

INSERT INTO rol(nombre, descripcion)
VALUES ('colaborador', 'No puede editar el proyecto, pero si su contenido');

INSERT INTO reqXcaso(requerimiento, casoUso)
VALUES (1, 1);

INSERT INTO usuarioXreq(usuario, requerimiento)
VALUES (1, 1);

INSERT INTO usuarioXproyecto(usuario, proyecto, rol)
VALUES (1, 1, 1);

INSERT INTO requerimientoXhistoria(requerimiento, historia)
VALUES (1, 1);

INSERT INTO casoUsoXactor(casoUso, actor)
VALUES (1, 1);
