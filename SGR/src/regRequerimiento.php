<?php
	session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
	require_once('conexion.php');
	$nombreReq="";
	$descripcion="";
	$origen="";
	if(isset($_POST['idReq'])){
		$idReq=$_POST['idReq'];
		$sql="SELECT * FROM requerimiento WHERE id='$idReq'";
		$resultado = queryPSQL($sql);
		$requerimiento = pg_fetch_assoc($resultado);
		$nombreReq=$requerimiento['requerimiento'];
		$descripcion=$requerimiento['descripcion'];
		$origen=$requerimiento['origen'];
	}else{
		$idReq='';
	}
?>
<!DOCTYPE html>
<html lang="es">
	<head>		
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
     	<title>Registro de Requerimientos</title>
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css"/>
        <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	</head>
	<body>
		<?php require_once('navbar.html'); ?>
		<div class="container">
			<h2 class="text-center" style="margin-bottom: 25px;">Nuevo requerimiento</h2>
			<form action="guardaRequerimiento.php" method="post">
				<legend>Ingresa los datos del requerimiento</legend>
				<div class="row">
					<div class="form-group col-sm-12">
						<label class="control-label" for="req">Requerimiento:</label>
						<input class="form-control" name="requerimiento" type="text" value='<?php echo $nombreReq?>' required/>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-sm-12">
						<label class="control-label" for="descripcionReq">Descripción del Requerimiento:</label>
						<textarea class="form-control" rows="2" cols="50" name="descripcionReq" ><?php echo $descripcion?></textarea>
					</div>
				</div>	
				<div class="row">
					<div class="form-group col-sm-12">
						<label class="control-label" for="origen">Origen:</label>
						<input class="form-control" name="origen" type="text" value='<?php echo $origen?>' required/>
					</div>
				</div>
				<div class="form-row">
		           	<div class='form-group col-sm-1 offset-sm-10 align-self-end'>
	        			<button type="submit" name="guardar" class="btn btn-outline-success">Guardar</button>
	        		</div>
	        		<div class='form-group col-sm-1 align-self-end'>
	        			<a href="reqs.php"><button type="button" class="btn btn-outline-danger">Cancelar</button></a>
	        		</div>
	    		</div>
	    		<input type="hidden" name="idReq" value=<?php echo $idReq?>>
			</form>
		</div>
	</body>
</html>