<?php
session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
	require_once('conexion.php');
	$idHist=$_POST['idhist'];
	$sql="SELECT * FROM historiausuario WHERE id='$idHist'";
	$res = queryPSQL($sql);
	$historia = pg_fetch_assoc($res);
	$sql="SELECT * FROM criterioaceptacion WHERE historiausuario='$idHist'";
	$res = queryPSQL($sql);
	$nombre=$historia['titulo'];
	$descripcion=$historia['descripcion'];
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">

	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css"/>
    <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
  	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<title>Inicio</title>
</head>
<body>
	<nav class="navbar navbar-light bg-dark navbar-dark" style="margin-bottom: 25px;">
		<!--div class="container"-->
	  <a class="navbar-brand" href="index.php">SGR DGTIC</a>
	  <ul class="nav nav-pills">
	    <li class="nav-item dropdown">
	      <a style="color: white;" class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Cuenta</a>
	      <div class="dropdown-menu">
	        <a class="dropdown-item" href="#">Editar perfil</a>
	        <a class="dropdown-item" href="#">Cerrar sesión</a>
	      </div>
	    </li>
	  </ul>
	  <!--/div-->
	</nav>
	<main class="container">
		<h2 class="text-center">Editar Historias de Usuario</h2>
		<fieldset>
			<legend>Historias de usuario</legend>
			<form action="actualizaHistoriaUs.php" method="post">
			  <div class="form-group">
			    <label for="inputAddress">Nombre de la historia</label>
			    <input type="text" class="form-control" name="historia" id="Nombre" value="<?php echo $nombre; ?>" required>
			  </div>
			  <div class="form-group">
			    <label for="exampleFormControlTextarea1">Descripción de la historia de usuario</label>
			    <textarea class="form-control" name="descripcion" id="descripcion" rows="2"><?php echo $descripcion; ?></textarea>
			  </div>
			  <div class="form-group">
			    <label for="inputAddress">Criterios de aceptación</label>
			    <?php  while($criterios = pg_fetch_assoc($res)){?>
			    <input type="text" class="form-control" name="<?php echo $criterios['id']; ?>" id="criterio" value="<?php echo $criterios['descripcion']; ?>">
			    <?php  } ?>
			  </div>
			  <div class='form-group' style="margin-top: 25px;">
    			<input type="submit" class="btn btn-outline-success" id="Guardar" value="Guardar">
    			<a href="hists.php"><button type="button" class="btn btn-outline-danger">Cancelar</button></a>
    		</div>
    		<input type="hidden" name="idHist" value="<?php echo $idHist ?>">
			</form>
		</fieldset>
	</main>
</body>
</html>