<?php
	require_once('conexion.php');
	require_once('limpiar.php');
	session_start();
	$id=$_SESSION['proyecto'];
	$nombre = empty($_POST['nombre']) ? '' : limpia($_POST['nombre']);
	$nombre=strtolower($nombre);
	$descripcion = empty($_POST['descripcion']) ? '' : limpia($_POST['descripcion']);
	$sql="SELECT * FROM actor WHERE nombre='$nombre' AND proyecto='$id'";
	$res = queryPSQL($sql);
	$datosActor = pg_fetch_assoc($res);
	if(empty($datosActor)){
			$sql="INSERT INTO actor (nombre, descripcion, proyecto) VALUES ('$nombre','$descripcion','$id')";
			$resultado = queryPSQL($sql);
			$mensaje="EL ACTOR SE REGISTRÓ CORRECTAMENTE";
	}

?>

<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/css/materialize.min.css">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <script type="text/javascript" src="js/materialize.min.js"></script>
	<title>Registro Actor</title>
</head>
<body>
<?php
  	if (isset($mensaje)) :
    	?>
	    <div class="card-panel green">
	    	<?php
	        echo "$mensaje" ?><a href="casos.php">Casos de Uso</a>
	    </div>
    	<?php
     else :
    	?>
	    <div class="card-panel red">
	        El actor ya existe <a href="regActor.php">Registrar Actor</a>
	    </div>
    	<?php
  	endif;
  ?>
</body>
</html>