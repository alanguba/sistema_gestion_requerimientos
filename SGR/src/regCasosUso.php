<?php
	require_once('conexion.php');
	session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
	$idProy=$_SESSION['proyecto'];
	if (isset($_POST['guardar'])) {
		$titulo = $_POST['titulo'];
		$descripcion = $_POST['descripcion'];
		$precondicion = $_POST['precondicion'];
		$fprincipal = $_POST['flujoPrin'];
		$postcondicion = $_POST['postcondicion'];
		$estado = 'activo';
		$proyectoId = 1;

		$sql = "INSERT INTO casouso(titulo, descripcion, precondicion, fprincipal, postcondicion, estado, proyecto) VALUES ('" . $titulo . "', '" . $descripcion . "', '" . $precondicion . "', '" . $fprincipal . "', '" . $postcondicion . "', 'activo', '" . $proyectoId . "')";
		$res = queryPSQL($sql);
		if ($res) {
			$guardado = true;
		}
		header('Location: casos.php');
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="../css/bootstrap.css">
	
	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
  	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="../js/guardaCasoUso.js"></script>
	<title>Inicio</title>
</head>
<body>
	<?php require_once('navbar.html'); ?>
	<main class="container">
		<div class="row">
			<div class="col-sm-10">
				<h2>Registrar Casos de Uso</h2>
			</div>
			<div class="col-sm-2">
				<a href="casos.php"><button type="button" class="btn btn-outline-secondary" name="nuevo">Regresar</button></a>
			</div>
		</div>
		<fieldset>
			<form>
				<legend class="text-center">Caso de uso</legend>
				  <div class="form-group">
				    <label for="inputAddress">Título del caso de uso</label>
				    <input type="text" name="titulo" id="title" class="form-control">
				  </div>
				  <div class="form-group">
				    <label for="exampleFormControlTextarea1">Descripción del caso de uso</label>
				    <textarea class="form-control" name="descripcion" id="descripcion" rows="2"></textarea>
				  </div>
				  <div class="form-group">
				    <label for="inputAddress">Pre-condiciones</label>
				    <input type="text" class="form-control" id="precondicion" name="precondicion">
				  </div>
				  <div class="form-group">
				    <label for="exampleFormControlTextarea1">Flujo principal</label>
				    <textarea class="form-control" name="flujoPrin" id="flujoPrin" rows="2"></textarea>
				  </div>
				  <div class="form-group">
				    <label for="exampleFormControlTextarea1">Flujo alterno</label>
				    <textarea class="form-control" rows="2" id="FA" placeholder="Agregar mas con el boton"></textarea>
				    <input type="button" class="btn btn-outline-primary" id="agregarFA" value="Agregar otro flujo">

				  </div>
				  <div class="form-group">
				    <label for="exampleFormControlTextarea1">Flujo de excepción</label>
				    <textarea class="form-control" rows="2" id="FE" placeholder="Agregar mas con el boton"></textarea>
				    <input type="button" class="btn btn-outline-primary" id="agregarFE" value="Agregar otro flujo">
				  </div>
				  <div class="form-group">
				    <label for="inputAddress">Post-condiciones</label>
				    <input type="text" class="form-control" id="postcondicion" name="postcondicion">
				  </div>
				  <div class="form-group">
				    <label for="inputAddress">Notas</label>
				    <input type="text" class="form-control" id="nota" placeholder="Agregar mas con el boton">
				    <input type="button" class="btn btn-outline-primary" id="agregarNota" value="Agregar otra nota">
				    <input type="hidden" id="idProy" value=<?php echo $idProy?>>
				  </div>
			  <legend class="text-center">Actor</legend>
			  	<div class="form-group">
    			<label for="exampleFormControlSelect1">Selecciona a un actor</label>
    			<select class="form-control" id="SelectActor">
			      <option>---</option>
			    </select>
			    <input type="button" class="btn btn-outline-primary" id="agregarActor" value="Agregar otro actor">
			  </div>
			  <input type="button" class="btn btn-outline-success" id="guardar" value="Registrar">
			</form>
		</fieldset>
	</main>
</body>
</html>