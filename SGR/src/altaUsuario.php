<?php
	require_once('conexion.php');
	session_start();
	if (!isset($_SESSION["id"]) || $_SESSION['id']!=1) {
        header("Location: login.php");
	}
	$sql = "SELECT * FROM usuario WHERE estado='inactivo'";
	$resultado = queryPSQL($sql);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Dar de alta a Usuario</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="../js/bootstrap.js">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
</head>
<body>
	<?php require_once('navbar.html'); ?>
	<main class="container">
		<table class="table">
		  <thead>
		    <tr>
		      <th>#</th>
		      <th>Nombre</th>
		      <th>Apellido Paterno</th>
		      <th>Apellido Materno</th>
		      <th>correo</th>
		      <th></th>
		      <th></th>
		    </tr>
		  </thead>

		  <tbody>
		  	<?php
			while ($usuario = pg_fetch_assoc($resultado)):
			$id= $usuario['id'];
			$nombre= $usuario['nombre'];
			$apellido1=$usuario['apaterno'];
			$apellido2=$usuario['amaterno'];
			$correo=$usuario['correo'];
		 ?>
		    <tr>
		      <td><?php echo $id; ?></td>
		      <td><?php echo $nombre; ?></td>
		      <td><?php echo $apellido1; ?></td>
		      <td><?php echo $apellido2; ?></td>
		      <td><?php echo $correo; ?></td>
		      <td>
		      	<form action="activarUsuario.php" method="post">
		      	<input type="hidden" name="idUsuario" value=<?php echo $id?>>
		      	<button type="submit" class="btn btn-success">Dar de Alta</button>
		      	</form>
		      </td>
		      <td>
		      	<form action="quitarUsuario.php" method="post">
		      	<input type="hidden" name="idUsuario" value=<?php echo $id?>>
		      	<button type="submit" class="btn btn-danger">Rechazar</button>
		      	</form>
		      </td>
		    </tr>
		    <?php endwhile; ?>
		  </tbody>
		</table>
		
	</main>

</body>
</html>