<?php
	require_once('conexion.php');
	session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
	$idCaso = empty($_POST['idcaso']) ? '' : $_POST['idcaso'];
	$sql="SELECT * FROM casouso WHERE id='$idCaso'";
	$res = queryPSQL($sql);
	$casouso = pg_fetch_assoc($res);
	$titulo=$casouso['titulo'];
	$descripcion=$casouso['descripcion'];
	$precondicion=$casouso['precondicion'];
	$fprin=$casouso['fprincipal'];
	$postcondicion=$casouso['postcondicion'];
	$sql="SELECT * FROM flujoaltexc WHERE casouso='$idCaso'";
	$res = queryPSQL($sql);
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="../css/bootstrap.css">
	
	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
  	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<title>Inicio</title>
</head>
<body>
	<?php require_once('navbar.html'); ?>
	<main class="container">
		<div class="row">
			<div class="col-sm-10">
				<h2>Registrar Casos de Uso</h2>
			</div>
			<div class="col-sm-2">
				<a href="casos.php"><button type="button" class="btn btn-outline-secondary" name="nuevo">Regresar</button></a>
			</div>
		</div>
		<fieldset>
			<form action="actualizaCaso.php" method="post">
				<legend class="text-center">Caso de uso</legend>
				  <div class="form-group">
				    <label for="inputAddress">Título del caso de uso</label>
				    <input type="text" name="titulo" id="title" class="form-control" value="<?php echo $titulo; ?>">
				  </div>
				  <div class="form-group">
				    <label for="exampleFormControlTextarea1">Descripción del caso de uso</label>
				    <textarea class="form-control" name="descripcion" id="descripcion" rows="2"><?php echo $descripcion; ?></textarea>
				  </div>
				  <div class="form-group">
				    <label for="inputAddress">Pre-condiciones</label>
				    <input type="text" class="form-control" id="precondicion" name="precondicion" value="<?php echo $precondicion; ?>">
				  </div>
				  <div class="form-group">
				    <label for="exampleFormControlTextarea1">Flujo principal</label>
				    <textarea class="form-control" name="flujoPrin" id="flujoPrin" rows="2"><?php echo $fprin; ?></textarea>
				  </div>
				  <div class="form-group">
				    <label for="exampleFormControlTextarea1">Flujo alterno</label>
				    <?php while($flujos = pg_fetch_assoc($res)): 
				    	
				  		if($flujos['tipo']=='alterno'){
				  ?>
				    <textarea class="form-control" rows="2" id="FA" name=<?php echo $flujos['id']; ?>><?php echo $flujos['descripcion']; ?></textarea>
				<?php } ?>
				  </div>
				  <?php if($flujos['tipo']=='excepcion'){
				  ?>
				  <div class="form-group">
				    <label for="exampleFormControlTextarea1">Flujo de excepción</label>
				  		
				    <textarea class="form-control" name=<?php echo $flujos['id']; ?> rows="2" id="FE"><?php echo $flujos['descripcion']; ?></textarea>
				  </div>
				  <?php } endwhile; ?>
				  
				  <div class="form-group">
				    <label for="inputAddress">Post-condiciones</label>
				    <input type="text" class="form-control" id="postcondicion" name="postcondicion" value="<?php echo $postcondicion; ?>">
				  </div>
			  <input type="submit" class="btn btn-outline-success" id="guardar" value="Siguiente" >
			  <input type="hidden" name="idCaso" value=<?php echo $idCaso; ?>>
			</form>
		</fieldset>
	</main>
</body>
</html>