<?php
	require_once('conexion.php');
	$mensaje='';
	if (!empty($_GET['mensaje'])) {
    	$mensaje = $_GET['mensaje'];
	}

	if (!empty($_POST['enviar'])) {
  		$usuario = empty($_POST['email']) ? '' : $_POST['email'];
    	$password = empty($_POST['pass']) ? '' : $_POST['pass'];

	if(empty($usuario) || empty($password)){
		$mensaje = 'Debes colocar el usuario y la contraseña';
  		$mensaje = urlencode($mensaje);
	  	header('Location: login.php?mensaje='. $mensaje);
	}
	else {
         $sql = <<<EOT
        SELECT 
          *
        FROM 
          usuario
        WHERE
          correo = '$usuario' AND
          password = '$password'
;   
EOT;
$res = queryPSQL($sql);
$datosUsuario = pg_fetch_assoc($res);

  if (!empty($datosUsuario)) {
	  	if($datosUsuario['estado']=='activo' || $datosUsuario['estado']=='admin'){
	  		session_start();
	        $_SESSION['correo'] = $usuario;
	        $_SESSION['id'] = $datosUsuario['id'];
	        header('Location: index.php');
	  	}else{
	  		$mensaje = 'El usuario todavía no está dado de alta por el administrador';
            $mensaje = urlencode($mensaje);
	  		header('Location: login.php?mensaje='. $mensaje);
	  	}       
        } else {
          	$mensaje = 'El nombre de usuario o contraseña es incorrecto';
            $mensaje = urlencode($mensaje);
	  		header('Location: login.php?mensaje='. $mensaje);
        }
	}
}
?>
<!DOCTYPE html>
<html lang="es">
    <head>
     <head>		
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
     	<title>Acceso</title>
     	<link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/> 
		<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
    	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../js/login.js"></script>
    </head>
    <body>
    	<?php   
			if (!empty($mensaje)) :
		?>

		<div class="bg-danger">
		<?php   
			echo $mensaje;
		?>
		</div>
		<?php
			endif;
		?>
    	<nav class="navbar navbar-light bg-dark navbar-dark" style="margin-bottom: 25px;">
    		<a class="navbar-brand" href="">DGTIC</a>
			<div class="container text-center">
				<a class="navbar-brand" href="">Sistema de Gestión de Requerimientos</a>
			</div>
		</nav>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2 style="margin-bottom: 25px;">Acceso y registro de usuarios</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-1 offset-sm-4 text-center">
					<button type="button" class="btn btn-outline-primary" id="ingreso" data-toggle="modal" data-target="#myModalIng">Ingreso</button>
				</div>
				<div class="col-sm-1 offset-sm-2 text-center">
					<button type="button" class="btn btn-outline-primary" id="registro" data-toggle="modal" data-target="#myModalReg">Registro</button>
				</div>
			</div>
		</div>
		<div class='modal fade' id='myModalIng' role='dialog'>
			<div class='modal-dialog'>
				<div class='modal-content'>
					<div class='modal-header'>
						<h4 class='modal-title'>Ingresar</h4>
						<button type='button' class='close' data-dismiss='modal'>&times;</button>
					</div>
					<div class='modal-body'>
						
						<form method="post" action="login.php">
							<div class="row">
								<div class="form-group col-sm-12">
									<label class="control-label" for="email">Correo:</label>
									<input type='email' name='email' class="form-control"/>
								</div>
							</div>	
							<div class="row">
								<div class="form-group col-sm-12">
									<label class="control-label" for="pass">Password:</label>
									<input type='password' name='pass' class="form-control"/>
								</div>
							</div>	
							<div class="row">
								<div class="form-group col-sm-3 offset-sm-9">
									<input type="submit" name="enviar" class="btn btn-success btn-outline-primary" value="Ingresar">
								</div>
							</div>
						</form>
					</div>
					<div class='modal-footer'>
						<a href="#">¿Olvidaste tu contraseña</a>
					</div>
				</div>
			</div> 
		</div>
		<div class='modal fade' id='myModalReg' role='dialog'>
			<div class='modal-dialog'>
				<div class='modal-content'>
					<div class='modal-header'>
						<h4 class='modal-title'>Registrarse</h4>
						<button type='button' class='close' data-dismiss='modal'>&times;</button>
					</div>
					<div class='modal-body'>
						<form action="registraUsuario.php" method="post">
							<div class="row">
								<div class="form-group col-sm-12">
									<label class="control-label" for="nombre">Nombre(s):</label>
									<input type='text' name='nombre' class="form-control" required/>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-12">
									<label class="control-label" for="aPaterno">Primer apellido:</label>
									<input type='text' name='aPaterno' class="form-control" required />
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-12">
									<label class="control-label" for="aMaterno">Segundo apellido:</label>
									<input type='text' name='aMaterno' class="form-control" required/>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-sm-12">
									<label class="control-label" for="email">Correo electrónico:</label>
									<input type='email' name='email' class="form-control" required/>
								</div>
							</div>	
							<div class="row">
								<div class="form-group col-sm-12">
									<label class="control-label" for="pass">Contraseña:</label>
									<input type='password' name='pass' class="form-control" required/>
								</div>
							</div>	
							<div class="row">
								<div class="form-group col-sm-12">
									<label class="control-label" for="passCon">Confirma la contraseña:</label>
									<input type='password' name='passCon' class="form-control" required/>
								</div>
							</div>	
							<input id="tipo" name="tipo" type="hidden" value="colaborador">
							<div class="row">
								<div class="form-group col-sm-3 offset-sm-9">
									<button type='submit' class='btn btn-success btn-outline-primary'>Registrar</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div> 
		</div>
	</body>
</html>