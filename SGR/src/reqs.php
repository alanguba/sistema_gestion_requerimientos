<?php
	require_once('conexion.php');
	session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
	$id=$_SESSION['proyecto'];
	$proy = empty($_POST['nomProy']) ? '' : $_POST['nomProy'];
	$sql = "SELECT * FROM requerimiento where proyecto='$id' AND estado='activo'";
	$resultado = queryPSQL($sql);
?>
<!DOCTYPE html>
<html lang="es">
	<head>		
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   	<title>Requerimientos</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css"/>
    <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	</head>
	<body>
		<?php require_once('navbar.html'); ?>
		<div class="container">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<legend>Requerimientos del Proyecto <?php echo $proy; ?></legend>
					</div>
					<div class="col-sm-4">
						<?php if($_SESSION['permiso']){?>
						<a href="regRequerimiento.php"><button type="button" class="btn btn-outline-success" name="nuevo">Nuevo requerimiento</button></a>
						<?php }?>
						<a href="proyecto.php?id=<?php echo $id?>"><button type="button" class="btn btn-outline-secondary" name="nuevo">Regresar</button></a>
					</div>
				</div>
				<table class="table" style="margin-top: 25px;">
	  				<thead>
	    				<tr>
	      					<th scope="col">#</th>
				    		<th scope="col">Requerimiento</th>
							<th scope="col">Descripción</th>
							<th scope="col">Origen</th>
							<th scope="col"></th>
							<th scope="col"></th>
							<th scope="col"></th>
	    				</tr>
	  				</thead>
	  				<tbody>
	  					<?php while($requerimientos = pg_fetch_assoc($resultado)): 
	  						$id= $requerimientos['id'];
	  						$req=$requerimientos['requerimiento'];
	  						$desc=$requerimientos['descripcion'];
	  						$origen=$requerimientos['origen'];
	  					?>
	    				<tr>
					     	<td><?php echo $id ?></td>
					      	<td><?php echo $req ?></td>
					      	<td><?php echo $desc ?></td>
					      	<td><?php echo $origen ?></td>
					      	<td>
					      		<form action="requerimientosDetalle.php" method="post">
					      			<input type="hidden" name="idreq" value=<?php echo $id;?>>
					      			<input type="hidden" name="nomProy" value=<?php echo $proy;?>>
					      			<input type="submit" class="btn btn-outline-info" name="detalles" value="Detalles">
					      		</form>
					      	</td>
					      	<td>
					      		<?php if($_SESSION['permiso']){?>
					      		<form action="regRequerimiento.php" method="post">
					      			<input type="hidden" name="idReq" value=<?php echo $id;?>>
					      			<input type="submit" class="btn btn-outline-primary" name="editar" value="Editar">
					      		</form>
					      		<?php }?>
					      	</td>
					      	<td>
					      		<?php if($_SESSION['permiso']){?>
					      		<form action="eliminaReq.php" method="post">
					      			<input type="hidden" name="idReq" value=<?php echo $id;?>>
					      			<input type="submit" class="btn btn-outline-danger" name="borrar" value="Borrar">
					      		</form>
					      		<?php }?>
					      	</td>
					    </tr>
					    <?php endwhile; ?>
	  				</tbody>
				</table>
			</div>
		</div>
	</body>
</html>