<?php
	require_once('conexion.php');
	session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
	$id=$_SESSION['proyecto'];
	$proy = empty($_POST['nomProy']) ? '' : $_POST['nomProy'];

	$sql = "SELECT * FROM casouso WHERE proyecto='$id' AND estado='activo'";
	$resultado = queryPSQL($sql);

	$sql="SELECT * FROM actor WHERE proyecto='$id'";
	$resultado2 = queryPSQL($sql);
?>
<!DOCTYPE html>
<html lang="es">
	<head>		
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   	<title>Requerimientos</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css"/>
    <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	</head>
	<body>
			<?php require_once('navbar.html'); ?>
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<legend>Casos de uso del Proyecto <?php echo $proy ?></legend>
					</div>
					<div class="col-sm-4">
						<?php if($_SESSION['permiso']){?>
						<a href="regCasosUso.php"><button type="button" class="btn btn-outline-success" name="nuevo">Nuevo caso de uso</button></a>
						<?php }?>
						<a href="proyecto.php?id=<?php echo $id?>"><button type="button" class="btn btn-outline-secondary" name="nuevo">Regresar</button></a>
					</div>
				</div>

				<div class="table-responsive">
					<table class="table" style="margin-top: 25px;">
	  				<thead class="thead-light">
	    				<tr>
	      				<th scope="col">#</th>
					    	<th scope="col">Título</th>
					    	<th scope="col">Descripción</th>
					    	<th scope="col"></th>
							<th scope="col"></th>
							<th scope="col"></th>
	    				</tr>
	  				</thead>
	  				<tbody>
	  					<?php while($row = pg_fetch_assoc($resultado)): ?>
	    				<tr>
					     	<td><?= $row['id'];?></td>
					      	<td><?= $row['titulo']; ?></td>
					      	<td><?= $row['descripcion']; ?></td>
					      	<td>
					      		<form action="casosDetalle.php" method="post">
					      			<input type="hidden" name="nomProyecto" value=<?php echo $proy; ?>>
					      			<input type="hidden" name="idcaso" value=<?php echo $row['id']; ?>>
					      			<input type="submit" class="btn btn-outline-info" name="detalles" value="Detalles">
					      		</form>
					      	</td>
				      		<td>
				      			<?php if($_SESSION['permiso']){?>
				      			<form action="editaCaso.php" method="post">
				      			<input type="hidden" name="idcaso" value=<?php echo $row['id']; ?>>
				      			<button type="submit" class="btn btn-outline-primary" name="editar">Editar</button>
				      			</form>
				      			<?php }?>
				      		</td>
				      		
				      		<td>
				      			<?php if($_SESSION['permiso']){?>
				      			<form action="eliminaCasos.php" method="post">
				      				<input type="hidden" name="idcaso" value=<?php echo $row['id']; ?>>
				      				<button type="submit" class="btn btn-outline-danger" name="borrar">Borrar</button>
				      			</form>
				      			<?php }?>
				      		</td>
				      		
					    </tr>
					    <?php endwhile; ?>
	  				</tbody>
				</table>
				</div>
				<div class="row">
					<div class="col-sm-8">
						<legend>Actores del Proyecto <?php echo $proy ?></legend>
					</div>
					<div class="col-sm-4">
						<?php if($_SESSION['permiso']){?>
						<a href="regActor.php"><button type="button" class="btn btn-outline-success" name="nuevo">Nuevo Actor</button></a>
						<?php }?>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table" style="margin-top: 25px;">
	  				<thead class="thead-light">
	    				<tr>
	      					<th scope="col">#</th>
					    	<th scope="col">Actor</th>
					    	<th scope="col">Descripción</th>
					    	<th scope="col"></th>
	    				</tr>
	  				</thead>
	  				<tbody>
	  					<?php while($row = pg_fetch_assoc($resultado2)): ?>
	    				<tr>
					     	<td><?= $row['id'];?></td>
					      	<td><?= $row['nombre']; ?></td>
					      	<td><?= $row['descripcion']; ?></td>
				      		<td>
				      			<?php if($_SESSION['permiso']){?>
				      			<form action="eliminaActor.php" method="post">
				      				<input type="hidden" name="idactor" value=<?php echo $row['id']; ?>>
				      				<button type="submit" class="btn btn-outline-danger" name="borrar">Borrar</button>
				      			</form>
				      			<?php }?>
				      		</td>
					    </tr>
					   <?php endwhile; ?>
	  				</tbody>
				</table>
				</div>
			</div>
			
	</body>
</html>