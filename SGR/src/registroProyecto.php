<?php
	require_once('conexion.php');
	session_start();
	$nombre=empty($_POST['nombre']) ? '' : addslashes($_POST['nombre']);
	$descripcion=empty($_POST['descripcion']) ? '' : addslashes($_POST['descripcion']);
	$objetivo=empty($_POST['objetivo']) ? '' : addslashes($_POST['objetivo']);
	$inicio=empty($_POST['inicio']) ? '' : $_POST['inicio'];
	$fin=empty($_POST['fin']) ? '' : $_POST['fin'];
	$idUs=$_SESSION['id'];
	$idProy = empty($_POST['idProy']) ? '' : $_POST['idProy'];

	if($idProy!=''){
		$sql="UPDATE proyecto SET nombre='$nombre', descripcion='$descripcion',objetivo='$objetivo', finicio ='$inicio', ftermino='$fin' WHERE id='$idProy'";
		$res = queryPSQL($sql);
		if ($res) {
			header('Location: index.php');
		}
	}else{
		if(!empty($nombre) && !empty($descripcion)){
		$sql = "INSERT INTO proyecto(nombre, descripcion, objetivo, fInicio, fTermino, estado) VALUES ('$nombre','$descripcion','$objetivo','$inicio','$fin','activo')";
		$res = queryPSQL($sql);
		$sql="SELECT MAX(id)  FROM proyecto";
		$res = queryPSQL($sql);
		$proyecto = pg_fetch_assoc($res);
		$idProy=$proyecto['max'];
		$sql="INSERT INTO usuarioXproyecto(usuario, proyecto, rol) VALUES ('$idUs', '$idProy', 1)";
		$res = queryPSQL($sql);
		$sql="INSERT INTO usuarioXproyecto(usuario, proyecto, rol) VALUES (1, '$idProy', 1)";
		$res = queryPSQL($sql);

		}else{
			//$res=false;
		}
	}
	
?>
<!DOCTYPE html>
<html lang="es">
	<head>		
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 		<title>Registro de Proyecto</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css"/>
    <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/guardaRoles.js"></script>
	</head>
	<body>
		<?php require_once('navbar.html'); ?>
		<main class="container">
			<legend class="text-center">Registra los roles en tu proyecto</legend>
				<div class="form-group">
					<label for="inputAddress">Ingresa el nombre</label>
					<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ej. Probador, Ingerniero en requerimientos, ...">
				</div>
				<div class="form-group">
					<label for="exampleFormControlTextarea1">Descripción</label>
					<textarea class="form-control" rows="2" id="descripcion" name="descripcion" placeholder="Ingresa la Descripción"></textarea>
				</div>
				<input type="hidden" name="idProy" id="idProy" value=<?php echo $idProy?>>
				<input type="button" class="btn btn-outline-primary" id="otro" value="Añadir otro">
				<input type="submit" class="btn btn-outline-success" id="guardar" value="Registrar">
	</main>
	</body>
</html>