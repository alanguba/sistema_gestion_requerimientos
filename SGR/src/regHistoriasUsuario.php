<?php
session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">

	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css"/>
    <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
  	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="../js/guardaHistoria.js"></script>
	<title>Inicio</title>
</head>
<body>
	<nav class="navbar navbar-light bg-dark navbar-dark" style="margin-bottom: 25px;">
		<!--div class="container"-->
	  <a class="navbar-brand" href="index.php">SGR DGTIC</a>
	  <ul class="nav nav-pills">
	    <li class="nav-item dropdown">
	      <a style="color: white;" class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Cuenta</a>
	      <div class="dropdown-menu">
	        <a class="dropdown-item" href="#">Editar perfil</a>
	        <a class="dropdown-item" href="#">Cerrar sesión</a>
	      </div>
	    </li>
	  </ul>
	  <!--/div-->
	</nav>
	<main class="container">
		<h2 class="text-center">Registrar Historias de Usuario</h2>
		<fieldset>
			<legend>Historias de usuario</legend>
			<form action="guardaHistoriaUs.php" method="post">
			  <div class="form-group">
			    <label for="inputAddress">Nombre de la historia</label>
			    <input type="text" class="form-control" name="historia" id="Nombre" required>
			  </div>
			  <div class="form-group">
			    <label for="exampleFormControlTextarea1">Descripción de la historia de usuario</label>
			    <textarea class="form-control" name="descripcion" id="descripcion" rows="2"></textarea>
			  </div>
			  <div class="form-group">
			    <label for="inputAddress">Criterio de aceptación</label>
			    <input type="text" class="form-control" name="criterio" id="criterio" placeholder="Agregar mas con el boton" required>
			    <input type="button" class="btn btn-outline-primary" id="agregarCriterio" value="Agregar otro criterio">
			  </div>
			  <div class='form-group' style="margin-top: 25px;">
    			<input type="button" class="btn btn-outline-success" id="Guardar" value="Guardar">
    			<a href="hists.php"><button type="button" class="btn btn-outline-danger">Cancelar</button></a>
    		</div>
			</form>
		</fieldset>
	</main>
</body>
</html>