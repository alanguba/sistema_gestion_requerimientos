<?php
	require_once('conexion.php');
	session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
	$id = $_SESSION['proyecto'];
	$proy = empty($_POST['nomProy']) ? '' : $_POST['nomProy'];
	$sql = "SELECT * FROM historiaUsuario where proyecto='$id'";
	$resultado = queryPSQL($sql);

?>
<!DOCTYPE html>
<html lang="es">
	<head>		
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   	<title>Requerimientos</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css"/>
    <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	</head>
	<body>
		<?php require_once('navbar.html'); ?>
		<div class="container">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<legend>Historias de usuarios del Proyecto <?php echo $proy ?></legend>
					</div>
					<div class="col-sm-4">
						<?php if($_SESSION['permiso']){?>
						<a href="regHistoriasUsuario.php"><button type="button" class="btn btn-outline-success" name="nuevo">Nueva historia de usuario</button></a>
						<?php }?>
						<a href="proyecto.php?id=<?php echo $id?>"><button type="button" class="btn btn-outline-secondary" name="nuevo">Regresar</button></a>
					</div>
				</div>
				<table class="table table-responsive" style="margin-top: 25px;">
	  				<thead>
	    				<tr>
	      			<th scope="col">#</th>
				    	<th scope="col">Historia</th>
							<th scope="col">Descripción</th>
							<th scope="col">Criterios de aceptación</th>
							<th scope="col"></th>
							<th scope="col"></th>
	    				</tr>
	  				</thead>
	  				<tbody>
	  					<?php while($historias = pg_fetch_assoc($resultado)): 
	  						$idHist= $historias['id'];
	  						$historia=$historias['titulo'];
	  						$desc=$historias['descripcion'];
	  						$sql2="SELECT descripcion FROM criterioAceptacion where historiausuario='$idHist'";
	  						$resultado2 = queryPSQL($sql2);
	  					?>
	    				<tr>
					     	<td><?php echo $idHist ?></td>
					      	<td><?php echo $historia ?></td>
					      	<td><?php echo $desc ?></td>
					      	<td>
					      	<?php while($criterios = pg_fetch_assoc($resultado2)): ?>
					      		<p><?php echo $criterios['descripcion'] ?></p>
					      	<?php endwhile; ?>
					      </td>
					      	<td>
					      		<?php if($_SESSION['permiso']){?>
					      		<form action="editaHist.php" method="post">
					      		<input type="hidden" name="idhist" value=<?php echo $idHist; ?>>
					      		<button type="submit" class="btn btn-outline-primary" name="editar">Editar</button>
					      		</form>
					      		<?php }?>
					      	</td>
					      	<td>
					      		<?php if($_SESSION['permiso']){?>
					      		<form action="eliminaHist.php" method="post">
					      			<input type="hidden" name="idhist" value=<?php echo $idHist; ?>>
					      			<button type="submit" class="btn btn-outline-danger" name="borrar">Borrar</button>
					      		</form>
					      		<?php }?>
					      	</td>
					    </tr>
					   <?php endwhile; ?>
	  				</tbody>
				</table>
			</div>
		</div>
	</body>
</html>