<?php
	require_once('conexion.php');
	session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
	$idReq = empty($_POST['idreq']) ? '' : $_POST['idreq'];
	$sql = "SELECT * FROM requerimiento where id='$idReq'";
	$resultado = queryPSQL($sql);
	$requerimiento = pg_fetch_assoc($resultado);
	$sql="SELECT * FROM reqxcaso WHERE requerimiento='$idReq'";
	$resultado = queryPSQL($sql);
	$sql="SELECT * FROM requerimientoxhistoria WHERE requerimiento='$idReq'";
	$resultado2=queryPSQL($sql);
	$proy = empty($_POST['nomProy']) ? '' : $_POST['nomProy'];
	$sql="SELECT nombre FROM usuario AS u INNER JOIN usuarioxreq AS ur ON u.id=ur.usuario WHERE ur.requerimiento='$idReq'";
	$resultado4=queryPSQL($sql);
	$usuarios = pg_fetch_assoc($resultado4);
?>

<!DOCTYPE html>
<html lang="es">
	<head>		
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   	<title>Requerimientos</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css"/>
    <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
	</head>
	<body>
		<?php require_once('navbar.html'); ?>
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<form action="reqs.php" method="post">
						<input type="hidden" name="nomProy" value=<?php echo $proy;?>>
					<input type="submit" class="btn btn-outline-secondary" name="nuevo" value="Regresar">
					</form>
				</div>
			</div>
			<table class="table table-striped" style="margin-top: 20px;">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">Requerimiento</th>
			      <th></th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <th scope="row">Id</th>
			      <td><?= $requerimiento['id'];?></td>
			    </tr>
			    <tr>
			      <th scope="row">Nombre</th>
			      <td><?= $requerimiento['requerimiento']; ?></td>
			      </tr>
			    <tr>
			      <th scope="row">Descripcion del requerimiento</th>
			      <td><?= $requerimiento['descripcion']; ?></td>
			    </tr>
			    <tr>
			      <th scope="row">Casos de Uso</th>
			      <td>
			      	<?php while($row = pg_fetch_assoc($resultado)): ?>
			      	<p><strong>
			      		<?php  
			      			$idCaso=$row['casouso'];
			      			$sql="SELECT * FROM casouso WHERE id='$idCaso'";
			      			$resultado1 = queryPSQL($sql);
			      			$casoUso = pg_fetch_assoc($resultado1);
			      			echo $casoUso['titulo'];
			      		?>
			      		</strong>
			      	</p>
			      	<p>
			      		<?php
			      			echo "Descripción: ".$casoUso['descripcion'];
			      		?>
			      	</p>
			      	<?php endwhile; ?>
			      </td>
			    </tr>
			    <tr>
			      <th scope="row">Historias de Usuario</th>
			      <td>
			      	<?php while($row = pg_fetch_assoc($resultado2)): ?>
			      	<p><strong>
			      		<?php  
			      			$idHistoria=$row['historia'];
			      			$sql="SELECT * FROM historiausuario WHERE id='$idHistoria'";
			      			$resultado3 = queryPSQL($sql);
			      			$historias = pg_fetch_assoc($resultado3);
			      			echo $historias['titulo'];
			      		?>
			      		</strong>
			      	</p>
			      	<p>
			      		<?php
			      			echo "Descripción: ".$historias['descripcion'];
			      		?>
			      	</p>
			      	<?php endwhile; ?>
			      </td>
			    </tr>
			     <tr>
			      <th scope="row">Asignado a:</th>
			      <td><?= $usuarios['nombre']; ?></td>
			    </tr>
			  </tbody>
			</table>
		</div>
	</body>
</html>