<?php
	require_once('conexion.php');
	session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
	$id=$_SESSION['proyecto'];
	$sql = "SELECT * FROM requerimiento where proyecto='$id'";
	$resultado = queryPSQL($sql);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Relacionar</title>
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
</head>
<body>
	<?php require_once('navbar.html'); ?>
	<div class="container">

		<h2>Elegir el caso de uso a relacionar</h2>
		<table class="table table-bordeless" style="margin-bottom: 25px; margin-top: 25px">
				<thead class="thead-dark">
					<tr>
						<th scope="col">Id</th>
						<th scope="col">Requerimiento</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php while($requerimientos = pg_fetch_assoc($resultado)): 
	  					$idReq= $requerimientos['id'];
	  					$req=$requerimientos['requerimiento'];
	  				?>
	  				<tr>
	  					<td><?php echo $idReq ?></td>
					    <td><?php echo $req ?></td>
					    <td>
					    	<form action="elegirCasos.php" method="post">
					    		<input type="hidden" name="idReq" value=<?php echo $idReq ?>>
						    	<button type="submit" class="btn btn-success" aria-label="Left Align">
	  								<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
								</button>
							</form>
					    </td>
	  				</tr>
			    	<?php endwhile; ?>
				</tbody>
		</table>
		<div class="form-row">
		<div class='form-group col-sm-1 align-self-end'>
	        <a href=<?php echo "proyecto.php?id=".$id;?>><button type="button" class="btn btn-outline-danger">Cancelar</button></a>
	        </div>
	    </div>
	</div>
    
  </p>
</body>
</html>