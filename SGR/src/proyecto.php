<?php
	require_once('conexion.php');
	session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
	$id = empty($_GET['id']) ? '' : $_GET['id'];
	$_SESSION['proyecto']=$id;
	$idusuario=intval($_SESSION['id']);
	if((array_search($id, $_SESSION['proyectos']) >=0 && array_search($id, $_SESSION['proyectos'])!= false) || $_SESSION['id']=='1'){
		$_SESSION['permiso']=true;
	}else{
		$_SESSION['permiso']=false;
	}
	$sql = "SELECT * FROM proyecto WHERE id='$id'";
	$resultado = queryPSQL($sql);
	$proyecto = pg_fetch_assoc($resultado);
	$sql = "SELECT rol FROM usuarioxproyecto WHERE usuario='$idusuario' AND proyecto='$id'";
	$resultado2 = queryPSQL($sql);
	$rol = pg_fetch_assoc($resultado2);
?>
<!DOCTYPE html>
<html lang="es">
	<head>		
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   	<title>Requerimientos</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css"/>

    <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	</head>
	<body>
		<?php require_once('navbar.html'); ?>
		<div class="container">
			<div class="card" style="margin-bottom: 25px;">
			  <h3 class="card-header text-center">Proyecto <?php echo $proyecto['nombre']; ?></h3>
			  <div class="card-body">
			    <h4 class="card-title">Objetivo</h4>
			    <p class="card-text"><?php echo $proyecto['objetivo']; ?></p>
			    <h5 class="card-title">Descripción</h5>
			    <p class="card-text"><?php echo $proyecto['descripcion']; ?></p>
			    <div class="form-row">
      			<div class='form-group col-sm-6'>
      				<label class="control-label" for="FechaInicio">Inicio del proyecto:</label>
              <input type='text' id='fInicio' class="form-control" disabled value="<?php echo $proyecto['finicio']; ?>" />
          	</div>
          	<div class='form-group col-sm-6'>
          		<label class="control-label" for="FechaTermino">Termino del proyecto:</label>
              <input type='text' id='fTermino' class="form-control" disabled value="<?php echo $proyecto['ftermino']; ?>" />
           	</div>
	    		</div>
	    		<div class="row" style="margin-top: 25px">
						<div class="col-sm-3 offset-sm-1">
							<form action="reqs.php" method="post">
								<input type="hidden" name="idproyecto" value=<?php echo $id; ?>>
								<input type="hidden" name="nomProy" value=<?php echo $proyecto['nombre']; ?>>
								<input type="submit"  class="btn btn-outline-secondary" value="Ver requerimientos">
							</form>
						</div>
						<div class="col-sm-3 offset-sm-1">
							<form action="hists.php" method="post">
							<input type="hidden" name="idproyecto" value=<?php echo $id; ?>>
							<input type="hidden" name="nomProy" value=<?php echo $proyecto['nombre']; ?>>
							<input type="submit"  class="btn btn-outline-secondary" value="Ver historias de usuario">
							</form>
						</div>
						<div class="col-sm-3 offset-sm-1">
							<form action="casos.php" method="post">
							<input type="hidden" name="idproyecto" value=<?php echo $id; ?>>
							<input type="hidden" name="nomProy" value=<?php echo $proyecto['nombre']; ?>>
							<input type="submit" class="btn btn-outline-secondary" value="Ver modelos casos de uso">
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="container row" >
				
				<?php if($_SESSION['permiso']){?>
				<form action="trazabilidad.php" method="post">
					<div class="col-sm-4 offset-sm-5">
					<input type="hidden" name="idproyecto" value=<?php echo $id; ?>>
					<input type="submit" class="btn btn-outline-secondary" value="Agregar Relaciones">
					</div>
				</form>
				<?php }?>
				<?php if($_SESSION['permiso'] && $rol['rol']==1){?>
				<form action="regProyecto.php" method="post">
					<div class="col-sm-4 offset-sm-5">
					<input type="hidden" name="idproyecto" value=<?php echo $id; ?>>
					<input type="submit" class="btn btn-outline-primary" value="Editar Proyecto">
					</div>
				</form>
				<form action="eliminaProyecto.php" method="post">
					<div class="col-sm-4 offset-sm-5">
					<input type="hidden" name="idproyecto" value=<?php echo $id; ?>>
					<input type="submit" class="btn btn-outline-danger" value="Eliminar Proyecto">
					</div>
				</form>
			<?php }?>
			</div>
			<?php
				$sql = "SELECT * FROM usuarioxproyecto WHERE proyecto='$id'";
				$resultado = queryPSQL($sql);
				//$row = pg_fetch_assoc($resultado);
				//var_dump($row);
			?>
			<table class="table table-bordeless" style="margin-bottom: 25px; margin-top: 25px">
				<thead class="thead-dark">
					<tr>
						<th scope="col">Colaboradores</th>
						<th scope="col"></th>
					</tr>
				</thead>
				<tbody>
					<?php
						while ($proyecto = pg_fetch_assoc($resultado)):
						$usuario= $proyecto['usuario'];
						$sql="SELECT * FROM usuario AS u INNER JOIN usuarioxproyecto AS up ON u.id=up.usuario WHERE id='$usuario' AND rol!=1 AND proyecto='$id'";
						$res = queryPSQL($sql);
						$row = pg_fetch_assoc($res);
						$nombre=$row['nombre'];
						if(pg_num_rows($res)>0){
					?>
					<tr>
			     	<td><?php echo $nombre; ?></td>
			      <td>
			      	<?php if($_SESSION['permiso'] && $rol['rol']==1){?>
			      	<form action="eliminaMiembro.php" method="post">
			      		<input type="hidden" name="idusuario" value=<?php echo $usuario; ?>>
			      		<button type="submit" class="btn btn-outline-danger" name="borrar">Borrar</button>
			      	</form>
			      	<?php }?>
			      </td>
			    </tr>
			    <?php } endwhile; ?>
			    <tr>
			    	<td scope="col">
			    		<?php if(($_SESSION['permiso'] && $rol['rol']==1) || $rol['rol']==1){?>
			    		<form action="agregarMiembro.php" method="post">
			    			<input type="hidden" name="idproyecto" value=<?php echo $id; ?>>
			    			<button type="submit" class="btn btn-outline-success" name="nuevo">Añadir</button>
			    		</form>
			    		<?php }?>
			    	</td>
			    </tr>
				</tbody>
			</table>
		</div>
	</body>
</html>
