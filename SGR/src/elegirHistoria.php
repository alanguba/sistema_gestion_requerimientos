<?php
	require_once('conexion.php');
	session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
	$id=$_SESSION['proyecto'];
	$casos=[];
	$idReq = empty($_POST['idReq']) ? '' : intval($_POST['idReq']);
	$sql = "SELECT * FROM casouso WHERE proyecto='$id'";
	$resultado = queryPSQL($sql);
	while($row = pg_fetch_assoc($resultado)):
		if(isset($_POST[$row['id']])){
			array_push($casos,$row['id']);
		}
	endwhile;
	$sql = "SELECT * FROM historiaUsuario where proyecto='$id'";
	$res = queryPSQL($sql);
	if(isset($_POST['siguiente'])){
		foreach ($casos as $value) {
			$value=intval($value);
			$sql="INSERT INTO reqxcaso (requerimiento,casouso) VALUES ('$idReq','$value')";
			$resultado = queryPSQL($sql);
		}
	}
?>

<!DOCTYPE html>
<html lang="es">
	<head>		
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   	<title>Requerimientos</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css"/>
    <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
	</head>
	<body>
		<nav class="navbar navbar-light bg-dark navbar-dark" style="margin-bottom: 25px;">
			<!--div class="container"-->
		  <a class="navbar-brand" href="index.html">SGR DGTIC</a>
		  <ul class="nav nav-pills">
		    <li class="nav-item">
		      <a style="color: white;" class="nav-link" href="#">Colaboradores</a>
		    </li>
		    <li class="nav-item dropdown">
		      <a style="color: white;" class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Cuenta</a>
		      <div class="dropdown-menu">
		        <a class="dropdown-item" href="#">Editar perfil</a>
		        <a class="dropdown-item" href="#">Cerrar sesión</a>
		      </div>
		    </li>
		  </ul>
		  <!--/div-->
		</nav>
		<div class="container">
			<div class="container">
				<div class="container">
				<div class='form-group col-sm-1 align-self-end'>
			<form action="elegirCasos.php" method="post">
				<input type="hidden" name="idReq" value=<?php echo $idReq ?>>
	        	<button type="submit" class="btn btn-outline-danger">Regresar</button>
	        </form>
	        </div>
				<form action="guardaRelacion.php" method="post">
					<h2>Elegir las historias de usuario relacionadas al requerimiento</h2>
				<table class="table table-responsive" style="margin-top: 25px;">
	  				<thead>
	    				<tr>
	      			<th scope="col">#</th>
				    	<th scope="col">Historia</th>
							<th scope="col">Descripción</th>
							<th scope="col">Criterios de aceptación</th>
							<th scope="col"></th>
	    				</tr>
	  				</thead>
	  				<tbody>
	  					<?php while($historias = pg_fetch_assoc($res)): 
	  						$idHist= $historias['id'];
	  						$historia=$historias['titulo'];
	  						$desc=$historias['descripcion'];
	  					?>
	    				<tr>
					     	<td><?php echo $idHist ?></td>
					      	<td><?php echo $historia ?></td>
					      	<td><?php echo $desc ?></td>
					      	<td>
					      		<input type="checkbox" name=<?php echo $idHist; ?> value=<?php echo $idHist; ?>>
					      	</td>
					    </tr>
					   <?php endwhile; ?>
	  				</tbody>
				</table>
				<input type="hidden" name="idReq" value=<?php echo $idReq ?>>
				<input type="submit" name="siguiente" class="btn btn-success" value="Siguiente">
				<input type="submit" name="omitir" class="btn btn-danger" value="Omitir">
				</form>
			</div>
		</div>
	</body>
</html>