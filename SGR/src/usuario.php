<?php
	require_once('conexion.php');
	$idusuario=$_POST['idUsuario'];
	$sql = "SELECT * FROM usuario WHERE id='$idusuario'";
	$resultado = queryPSQL($sql);
	$usuario= pg_fetch_assoc($resultado);
	$sql="SELECT proyecto FROM usuarioxproyecto WHERE usuario='$idusuario'";
	$resultado = queryPSQL($sql);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Usuario</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="../css/bootstrap.css">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5 class="card-title"><?php echo $usuario['nombre']." ".$usuario['apaterno']." ".$usuario['amaterno'] ;?></h5>
    <h6 class="card-subtitle mb-2 text-muted">Correo: <?php echo " ".$usuario['correo'];?></h6>
    <ul class="list-group list-group-flush">
    <li class="list-group-item"><strong>Proyectos en los que colabora</strong></li>
    <?php while($cantproy= pg_fetch_assoc($resultado)):
    	$idproy=$cantproy['proyecto']; 
    	$sql="SELECT nombre FROM proyecto WHERE id='$idproy'";
    	$resultado2 = queryPSQL($sql);
    	$proy= pg_fetch_assoc($resultado2);
    ?>
    	<li class="list-group-item"><?php echo $proy['nombre'];?></li>
    <?php endwhile;?>
  	</ul>
    <p class="card-text">Es una persona muy famosa.</p>
    <a href="index.php" class="card-link">Regresar</a>
  </div>
</div>

</body>
</html>