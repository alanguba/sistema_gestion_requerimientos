<?php 
	require_once('conexion.php');
	session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
	$id=$_SESSION['proyecto'];
	$idReq = empty($_POST['idReq']) ? '' : $_POST['idReq'];
	$sql = "SELECT * FROM casouso WHERE proyecto='$id'";
	$resultado = queryPSQL($sql);
?>

<!DOCTYPE html>
<html lang="es">
	<head>		
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   	<title>Requerimientos</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css"/>
    <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
	</head>
	<body>
			<?php require_once('navbar.html'); ?>
			<div class="container">
				<div class='form-group col-sm-1 align-self-end'>
	        <a href="trazabilidad.php"><button type="button" class="btn btn-outline-danger">Regresar</button></a>
	        </div>
				<form action="elegirHistoria.php" method="post">
				<div class="table-responsive">
					<h2>Elegir los casos de uso relacionados al requerimiento</h2>
					<table class="table" style="margin-top: 25px;">
	  				<thead class="thead-light">
	    				<tr>
	      				<th scope="col">#</th>
					    	<th scope="col">Título</th>
					    	<th scope="col">Descripción</th>
					    	<th scope="col"></th>
	    				</tr>
	  				</thead>
	  				<tbody>
	  					<?php while($row = pg_fetch_assoc($resultado)): ?>
	    				<tr>
					     	<td><?= $row['id'];?></td>
					      	<td><?= $row['titulo']; ?></td>
					      	<td><?= $row['descripcion']; ?></td>
					      	<td>
					      		<input type="checkbox" name=<?php echo $row['id']; ?> value=<?php echo $row['id']; ?>>
					      	</td>
					    </tr>
					    <?php endwhile; ?>
	  				</tbody>
				</table>
				</div>
				<input type="hidden" name="idReq" value=<?php echo $idReq ?>>
				<input type="submit" name="siguiente" class="btn btn-success" value="Siguiente">
				<input type="submit" name="omitir" class="btn btn-danger" value="Omitir">
				</form>
			</div>
			
	</body>
</html>