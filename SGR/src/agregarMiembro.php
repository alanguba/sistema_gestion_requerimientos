<?php
	require_once('conexion.php');
	session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
	$idProy=$_SESSION['proyecto'];
	$id=$_SESSION['id'];

	$sql = "SELECT * FROM usuario WHERE id!='$id' AND estado='activo'";
	$resultado = queryPSQL($sql);
?>
<!DOCTYPE html>
<html lang="es">
	<head>		
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	   	<title>Agregar Miembros</title>
	    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css"/>
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	    <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
	    <script type="text/javascript" src="../js/agregarMiembro.js"></script>
	</head>
	<body>
		<?php require_once('navbar.html'); ?>
		<div class="container">
		<div class="col-sm-8">
			<legend>Agrega miembros a tu proyecto</legend>
		</div>
		<div class='form-group col-sm-1 align-self-end'>
	        <a href="proyecto.php?id=<?php echo $idProy; ?>"><button type="button" class="btn btn-outline-danger">Regresar</button></a>
	       </div>
		<div class="table-responsive">
					<table class="table" style="margin-top: 25px;">
	  				<thead class="thead-light">
	    				<tr>
	      				<th scope="col">Usuario</th>
					    	<th scope="col"></th>
					    	<th scope="col"></th>
					    	<th scope="col">Correo</th>
							<th scope="col">Rol</th>
							<th scope="col"></th>
	    				</tr>
	  				</thead>
	  				<tbody>
	  					<?php while($row = pg_fetch_assoc($resultado)): 
	  						$idusuario=$row['id'];
	  						$sql="SELECT * FROM usuarioxproyecto WHERE proyecto='$idProy' AND usuario='$idusuario'";
	  						$res = queryPSQL($sql);
	  						if(pg_num_rows ($res)==0){
	  					?>
	    				<tr id=<?php echo $idusuario?>>
					     	<td><?= $row['nombre'];?></td>
					      	<td><?= $row['apaterno']; ?></td>
					      	<td><?= $row['amaterno']; ?></td>
					      	<td id="correo"><?= $row['correo']; ?></td>
					      	<td>
					      		<select class="form-control" id="SelectRol">
			      				<option>---</option>
			   	 				</select>
			   	 				<input type="hidden" id="idProy" value=<?php echo $idProy?>>
			   	 				<input type="hidden" id="idUs" value=<?php echo $idusuario?>>
			   	 			</td>
				      		<td>
						    	<button type="submit" id="guardar" class="btn btn-success" aria-label="Left Align">
	  								<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
								</button>
				      		</td>
				      		
					    </tr>
					    <?php }
						endwhile; 
						?>
	  				</tbody>
				</table>
				</div>
				</div>
	</body>
</html>