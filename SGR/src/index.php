<?php
	require_once('conexion.php');
	session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
	$sql = "SELECT * FROM proyecto WHERE estado='activo'";
	$resultado = queryPSQL($sql);
	$idUsuario=$_SESSION['id'];
	$sql="SELECT proyecto FROM usuarioxproyecto WHERE usuario='$idUsuario'";
	$res = queryPSQL($sql);
	$proyectos=[];
	array_push($proyectos, 0);
	while($row = pg_fetch_assoc($res)){
		array_push($proyectos, $row['proyecto']);
	}
	$_SESSION['proyectos']=$proyectos;

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="../js/bootstrap.js">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
	<title>Inicio</title>
</head>
<body>
	<?php require_once('navbar.html'); ?>
	<div class="container">
		<h2 class="text-center">Proyectos</h2>
		<h5>Bienvenido,<?php echo ' '.$_SESSION['correo']?></h5>
	</div>
	<div style="position:relative;" class="container text-right">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb bg-white">
		    
		  </ol>
		  <?php if($_SESSION['id']==1){ ?>
		  <a style="position:absolute; right:10rem; top:50%; transform:translateY(-50%);" class="btn btn-outline-primary" href="altaUsuario.php">Dar de alta a usuarios</a>
		<?php } ?>
		  <a style="position:absolute; right:1rem; top:50%; transform:translateY(-50%);" class="btn btn-outline-success" href="regProyecto.php">Crear proyecto</a>
		</nav>
	</div>
	<main class="container">
		<table class="table">
		  <thead>
		    <tr>
		      <th>#</th>
		      <th>Proyecto</th>
		      <th>Creador</th>
		    </tr>
		  </thead>

		  <tbody>
		  	<?php
			while ($proyecto = pg_fetch_assoc($resultado)):
			$id= $proyecto['id'];
			$nombre= $proyecto['nombre'];
			$sql="SELECT usuario FROM usuarioxproyecto WHERE proyecto='$id' AND rol=1";
			$res2 = queryPSQL($sql);
			$creadores = pg_fetch_assoc($res2);
			$idcreador=$creadores['usuario'];
			$sql="SELECT nombre FROM usuario WHERE id='$idcreador'";
			$res2 = queryPSQL($sql);
			$creador = pg_fetch_assoc($res2);
		 ?>
		    <tr>
		      <td><?php echo $id; ?></td>
		      <td><a href="proyecto.php?id=<?php echo $id; ?>"><?php echo $nombre; ?></a></td>
		      <td>
		      	<form action="usuario.php" method="post">
		      	<input type="hidden" name="idUsuario" value=<?php echo $idcreador?>>
		      	<button type="submit" class="btn btn-link"><?php echo $creador['nombre']; ?></button>
		      	</form>
		      </td>
		    </tr>
		    <?php endwhile; ?>
		  </tbody>
		</table>
		
	</main>
</body>
</html>