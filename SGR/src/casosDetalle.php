<?php
	require_once('conexion.php');
	session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
	$nomProy = empty($_POST['nomProyecto']) ? '' : $_POST['nomProyecto'];
	$idCaso = empty($_POST['idcaso']) ? '' : $_POST['idcaso'];
	$sql = "SELECT * FROM casouso WHERE id= '$idCaso'";
	$resultado = queryPSQL($sql);
	$casodeUso = pg_fetch_assoc($resultado);
	$sql = "SELECT * FROM flujoAltExc WHERE casouso= '$idCaso' and tipo='alterno'";
	$resultadoFA = queryPSQL($sql);
	$sql3 = "SELECT * FROM flujoAltExc WHERE casouso= '$idCaso' and tipo='excepcion'";
	$resultadoFE = queryPSQL($sql3);
	$sql2 = "SELECT * FROM nota WHERE flujo= '$idCaso'";
	$resultado2 = queryPSQL($sql2);
	$sql="SELECT actor FROM casousoxactor WHERE casouso='$idCaso'";
	$resultadoActs=queryPSQL($sql);
	$arregloNombres=[];
	while($actores = pg_fetch_assoc($resultadoActs)):
		$id=$actores['actor'];
		$sql="SELECT nombre FROM actor WHERE id='$id'";
		$nombres=queryPSQL($sql);
		$nombre=pg_fetch_assoc($nombres);
		array_push($arregloNombres, $nombre['nombre']);
	endwhile;

?>
<!DOCTYPE html>
<html lang="es">
	<head>		
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   	<title>Requerimientos</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css"/>
    <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/proyectoFinal.js"></script>
	</head>
	<body>
		<?php require_once('navbar.html'); ?>
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<legend>Proyecto <?php echo $nomProy ?></legend>
				</div>
				<div class="col-sm-3">
					<a href="regCasosUso.php"><button type="button" class="btn btn-outline-success" name="nuevo">Nuevo caso de uso</button></a>
					<a href="casos.php"><button type="button" class="btn btn-outline-secondary" name="nuevo">Regresar</button></a>
				</div>
			</div>
			<table class="table table-striped" style="margin-top: 20px;">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">Caso de uso del proyecto <?php echo $nomProy ?></th>
			      <th></th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <th scope="row">Id</th>
			      <td><?= $casodeUso['id'];?></td>
			    </tr>
			    <tr>
			      <th scope="row">Casos de uso</th>
			      <td><?= $casodeUso['titulo']; ?></td>
			      </tr>
			    <tr>
			      <th scope="row">Descripcion de casos de uso</th>
			      <td><?= $casodeUso['descripcion']; ?></td>
			    </tr>
			    <tr>
			      <th scope="row">Precondiciones</th>
			      <td><?= $casodeUso['precondicion']; ?></td>
			    </tr>
			    <tr>
			      <th scope="row">Flujo principal</th>
			      <td><?= $casodeUso['fprincipal']; ?></td>
			    </tr>
			    <tr>
			      <th scope="row">Flujo alterno</th>
			      <td>
			      	<?php while($fujsalternos = pg_fetch_assoc($resultadoFA)): ?>
					    <p><?php echo $fujsalternos['descripcion'] ?></p>
					<?php endwhile; ?>
			      </td>
			    </tr>
			    <tr>
			      <th scope="row">Flujos de excepción:</th>
			      <td>
			      	<?php while($fujsexc = pg_fetch_assoc($resultadoFE)): ?>
					    <p><?php echo $fujsexc['descripcion'] ?></p>
					<?php endwhile; ?>
			      </td>
			    </tr>
			     <tr>
			      <th scope="row">Post-condiciones:</th>
			      <td><?= $casodeUso['postcondicion']; ?></td>
			    </tr>
			    <tr>
			      <th scope="row">Estado:</th>
			      <td><?= $casodeUso['estado']; ?></td>
			    </tr>
			    <tr>
			      <th scope="row">Notas:</th>
			      <td>
			      	<?php while($notas = pg_fetch_assoc($resultado2)): ?>
					    <p><?php echo $notas['descripcion'] ?></p>
					<?php endwhile; ?>
			      </td>
			    </tr>
			    <tr>
			      <th scope="row">Actores:</th>
			      <td>
			      	<?php foreach ($arregloNombres as $valor ) { ?>
					    <p><?php echo $valor ?></p>
					<?php } ?>
			      </td>
			    </tr>
			  </tbody>
			</table>
		</div>
	</body>
</html>