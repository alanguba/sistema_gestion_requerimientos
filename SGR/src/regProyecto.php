<?php
	require_once('conexion.php');
	session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
	$nombreProy="";
	$descripcion="";
	$objetivo="";
	$fechaInicio="";
	$fechaTermino="";
	if(isset($_POST['idproyecto'])){
		$idProy=$_POST['idproyecto'];
		$sql="SELECT * FROM proyecto WHERE id='$idProy'";
		$resultado = queryPSQL($sql);
		$proyecto = pg_fetch_assoc($resultado);
		$nombreProy=$proyecto['nombre'];
		$descripcion=$proyecto['descripcion'];
		$objetivo=$proyecto['objetivo'];
		$fechaInicio=$proyecto['finicio'];;
		$fechaTermino=$proyecto['ftermino'];;
	}else{
		$idProy='';
	}
?>
<!DOCTYPE html>
<html lang="es">
	<head>		
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 		<title>Registro de Proyecto</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css"/>
    <script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
	</head>
	<body>
		<nav class="navbar navbar-light bg-dark navbar-dark" style="margin-bottom: 25px;">
			<!--div class="container"-->
		  <a class="navbar-brand" href="index.php">SGR DGTIC</a>
		  <ul class="nav nav-pills">
		    <li class="nav-item dropdown">
		      <a style="color: white;" class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Cuenta</a>
		      <div class="dropdown-menu">
		        <a class="dropdown-item" href="#">Editar perfil</a>
		        <a class="dropdown-item" href="#">Cerrar sesión</a>
		      </div>
		    </li>
		  </ul>
		  <!--/div-->
		</nav>
		<div class="container">
			<h2 class="text-center">Nuevo proyecto</h2>
			<form action="registroProyecto.php" method="post">
				<legend>Ingresa los datos del proyecto</legend>
				<div class="row">
					<div class="form-group col-sm-12">
						<label class="control-label" for="nombreProyecto">Nombre del proyecto:</label>
						<input class="form-control" name="nombre" id="nombre" type="text" value='<?php echo $nombreProy;?>' required/>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-sm-12">
						<label class="control-label" for="descripcionProyecto">Descripción del proyecto:</label>
						<textarea class="form-control" rows="2" cols="50" name="descripcion" required><?php echo $descripcion?></textarea>
					</div>
				</div>	
				<div class="row">	
					<div class="form-group col-sm-12">
						<label class="control-label" for="objetivoProyecto">Objetivo del proyecto:</label>
						<input class="form-control" id="objetivo" name="objetivo" type="text" value='<?php echo $objetivo;?>' required/>
					</div>
				</div>	
				<div class="form-row">
      		<div class='form-group col-sm-4'>
      			<label class="control-label" for="FechaInicio">Inicio del proyecto:</label>
            <input type='date' id='fInicio' name="inicio" class="form-control" value='<?php echo $fechaInicio;?>' required/>
          </div>
          <div class='form-group col-sm-4'>
          	<label class="control-label" for="FechaTermino">Termino del proyecto:</label>
            <input type='date' id='fTermino' name='fin' class="form-control" value='<?php echo $fechaTermino;?>'required/>
          </div>
          <div class='form-group col-sm-1 offset-sm-2 align-self-end'>
      			<button type="submit" class="btn btn-outline-success">Guardar</button>
      		</div>
      		<div class='form-group col-sm-1 align-self-end'>
      			<a href="index.php"><button type="button" class="btn btn-outline-danger">Cancelar</button></a>
      		</div>
	    	</div>
	    	<input type="hidden" name="idProy" value=<?php echo $idProy?>>	
			</form>
		</div>
	</body>
</html>