<?php
	session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
	require_once('conexion.php');
	$idCaso = empty($_GET['id']) ? '' : $_GET['id'];
	$idProy=$_SESSION['proyecto'];
	$sql="SELECT * FROM nota WHERE flujo='$idCaso'";
	$res = queryPSQL($sql);
	$sql="SELECT * FROM actor WHERE proyecto='$idProy' ";
	$res3 = queryPSQL($sql);
	$sql="SELECT * FROM casousoxactor AS ca INNER JOIN actor AS a ON ca.actor=a.id WHERE casouso='$idCaso' ";
	$res2 = queryPSQL($sql);
	$ids=[];
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
  	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<title>Inicio</title>
</head>
<body>
	<?php require_once('navbar.html'); ?>
	<main class="container">
		<div class="row">
			<div class="col-sm-10">
				<h2>Registrar Casos de Uso</h2>
			</div>
			<div class="col-sm-2">
				<a href="casos.php"><button type="button" class="btn btn-outline-secondary" name="nuevo">Regresar</button></a>
			</div>
		</div>
		<fieldset>
				<legend class="text-center">Caso de uso</legend>
			  <legend class="text-center">Actores del caso de uso</legend>
			  	<table class="table table-bordeless" style="margin-bottom: 25px; margin-top: 25px">
				<thead class="thead-dark">
					<tr>
						<th scope="col">Id</th>
						<th scope="col">Nombre</th>
						<th>Accion</th>
					</tr>
				</thead>
				<tbody>
					<?php while($actor = pg_fetch_assoc($res2)): 
	  					$nombre= $actor['nombre'];
	  					$idAct= $actor['id'];
	  					array_push($ids, $idAct);
	  				?>
	  				<tr>
	  					<td><?php echo $idAct ?></td>
					    <td><?php echo $nombre ?></td>
					    <td>
					    	<form action="quitaActor.php" method="post">
					    		<input type="hidden" name="idAct" value=<?php echo $idAct; ?>>
					    		<input type="hidden" name="idCaso" value=<?php echo $idCaso; ?>>
						    	<button type="submit" class="btn btn-danger" aria-label="Left Align">
	  								<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								</button>
							</form>
					    </td>
	  				</tr>
			    	<?php endwhile; ?>
				</tbody>
		</table>
		<legend class="text-center">Actores que puedes agregar al caso de uso</legend>
			  	<table class="table table-bordeless" style="margin-bottom: 25px; margin-top: 25px">
				<thead class="thead-dark">
					<tr>
						<th scope="col">Id</th>
						<th scope="col">Nombre</th>
						<th>Accion</th>
					</tr>
				</thead>
				<tbody>
					<?php while($actor = pg_fetch_assoc($res3)): 
						
	  					$nombre= $actor['nombre'];
	  					$idAct= $actor['id'];
	  					if(!in_array($idAct, $ids)){
	  				?>
	  				<tr>
	  					<td><?php echo $idAct ?></td>
					    <td><?php echo $nombre ?></td>
					    <td>
					    	<form action="agregarActor.php" method="post">
					    		<input type="hidden" name="idAct" value=<?php echo $idAct ?>>
					    		<input type="hidden" name="idCaso" value=<?php echo $idCaso; ?>>
						    	<button type="submit" class="btn btn-success" aria-label="Left Align">
	  								<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
								</button>
							</form>
					    </td>
	  				</tr>
			    	<?php } endwhile; ?>
				</tbody>
		</table>
		<div class="form-group">
			<label for="inputAddress">Notas</label>
			<form action="actualizaNota.php" method="post">
				<?php while($notas = pg_fetch_assoc($res)): ?>
				<input type="text" class="form-control" name=<?php echo $notas['id']; ?> value="<?php echo $notas['descripcion']; ?>">
				<input type="hidden" name="idCaso" value=<?php echo $idCaso; ?>>
				<?php endwhile; ?>
				<input type="submit" class="btn btn-success" name="guardar" value="Guardar cambios">
			</form>
		</div>
		
			
		</fieldset>
	</main>
</body>
</html>