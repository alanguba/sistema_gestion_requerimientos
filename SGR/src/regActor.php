<?php
session_start();
	if (!isset($_SESSION["id"])) {
        header("Location: login.php");
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="../css/bootstrap.css">
	
	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
  	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<title>Inicio</title>
</head>
<body>
	<?php require_once('navbar.html'); ?>
	<main class="container">
		<div class="row">
			<div class="col-sm-10">
				<h2>Registrar Casos de Uso</h2>
			</div>
			<div class="col-sm-2">
				<a href="casos.php"><button type="button" class="btn btn-outline-secondary" name="nuevo">Regresar</button></a>
			</div>
		</div>
		<form action="guardaActor.php" method="post">
			<legend class="text-center">Actor</legend>
				<div class="form-group">
					<label for="inputAddress">Ingresa el nombre</label>
					<input type="text" class="form-control" id="precondicion" name="nombre" placeholder="Nombre" required>
				</div>
				<div class="form-group">
					<label for="exampleFormControlTextarea1">Descripción</label>
					<textarea class="form-control" rows="2" name="descripcion" placeholder="Ingresa la Descripción" required></textarea>
				</div>
				<input type="submit" class="btn btn-outline-success" id="guardar" value="Registrar">
		</form>
	</main>
</body>
</html>