$(document).ready(function() {

	var flujoA=$('#agregarFA');
	var flujoE=$('#agregarFE');
	var notas=$('#agregarNota');
	var masactores=$('#agregarActor');
	var FA=$('#FA');
	var FE=$('#FE');
	var nota=$('#nota');
	var guardar=$('#guardar');
	arregloFA=[];
	arregloFE=[];
	arregloNotas=[];
	arregloA=[];
	var actor=$('#SelectActor');
	var idProy=$('#idProy').val();

	var data = {
		idProy: idProy,
	};

	$.ajax({
		type: "POST",
		dataType: "json",
		data: data,
		url: '../src/traerActores.php',
		success: function(data) {
			if(data){
				var i;
				console.log(data);
				for(i=0;i<data.length;i++){
					actor.append('<option>'+data[i]+'</option>');
				}
				//window.location.href = "../src/hists.php";
			}
		}
	});


	flujoA.click(function(){
		if(FA.val()!=''){
			arregloFA.push(FA.val());
			FA.val('');
		}
	});

	masactores.click(function(){
		var texto=$('#SelectActor option:selected').text();
		if(texto!='---'){
			arregloA.push(texto);
			$('#SelectActor option:selected').remove();
			console.log(arregloA);
		}
	});

	flujoE.click(function(){
		if(FE.val()!=''){
			arregloFE.push(FE.val());
			FE.val('');
		}
	});

	notas.click(function(){
		if(nota.val()!=''){
			arregloNotas.push(nota.val());
			nota.val('');
		}
	});

	guardar.click(function(){
		var titulo=$('#title').val();
		var descripcion=$('#descripcion').val();
		var precondicion=$('#precondicion').val();
		var flujoPrin=$('#flujoPrin').val();
		arregloFA.push(FA.val());
		arregloFE.push(FE.val());
		var postcondicion=$('#postcondicion').val();
		arregloNotas.push(nota.val());
		arregloA.push($('#SelectActor option:selected').text());

		var data = {
			titulo: titulo,
			descripcion: descripcion,
			precondicion: precondicion,
			flujoPrin: flujoPrin,
			arregloFA: arregloFA,
			arregloFE: arregloFE,
			arregloA:arregloA,
			postcondicion: postcondicion,
			arregloNotas: arregloNotas
		};

		$.ajax({
			type: "POST",
			dataType: "json",
			data: data,
			url: '../src/guardaCasoUso.php',
			success: function(data) {
				if(data){
					window.location.href = "../src/casos.php";
				}
			}
		});

	});
});