Gutiérrez Bañuelos Alan

####Requerimientos técnicos####
Manejador de Base de Datos: Postgres 
Versión del manejador de BD: PostgresSQL 9.6.10, compilado por Visual C++ 64-bit.

Necesita contar con el software Xampp

Versión de Xampp: 7.2.9

Versión de PHP: 7.2.9

Versión de Apache: 2.4.34

El nombre de la base de datos del proyecto es: sgr

Para crear la base de datos, dirijase a la terminal cmd, cree la base de datos y, si no tiene, cree un super usuario.

Primero debe ingresar a cualquier base de datos:
psql -U usuario basedeDatos

Introduzca la contraseña del usuario

Cree la base de datos

create database sgr;

Para ejecutar el código sql debe descargar el zip del repositorio y descomprimirlo, en la ruta C:\xampp\htdocs. 

salga de la base de datos que creó y ejecute el código sql proporcionado dentro de la carpeta sistema_gestion_requerimientos-master\docs\bd dirijase desde la terminal a esa posición cmd C:\xampp\htdocs\sistema_gestion_requerimientos-master\docs\bd y ejecute la instrucción:

'psql -U usuario sgr < sgr.sql'

En el archivo PHP.ini se deben habilitar las siguientes extensiones, descomentandolas:

Extensión=pgsql
Extensión=pdo_pgsql

Descomentarlas quitando el punto y coma al principio de cada extensión.

Para la configuración del email deberá abrir el archivo sendemail.ini dirigiendose a C:\xampp\sendmail\sendmail.ini.

comentar agregando ";" al rpincipio de la extensión "smtp_server=mail.mydomain.com" y agregar smtp_server=smtp.gmail.com además debe modificar el puesto, debajo está la linea "smtp_post=25" modificarla a smtp_port=587.

Dirigirse más abajo y agregar en las extensiones auth_username=correo de gmail y auth_password=contraseña.

Puede usar cualquier correo de gmail. Le brindamos uno hecho solo para fines de este sistema:
 auth_username=aluzulpahegu@gmail.com
auth_password=roaroaroa

En el PHP.ini comentar la linea:
sendmail_path="C:\xampp\mailtodisk\mailtodisk.exe" y agregar:
sendmail_path="\"C:\xampp\sendmail\sendmail.exe\" -t"

Al finalizar debe reiniciar el servidor.

Entre al archivo correo.php en la siguiente ruta: C:\xampp\htdocs\sistema_gestion_requerimientos-master\SGR\src y descomente la variable $correo, en la línea 3 y coloque el correo de su preferencia.


####Pasos para instalar#####
1.- El código se debe descargar desde el link del repositorio en formato zip.

2.- Se debe contar con el software xampp para correr el código. 

3.- Se deberá descomprimir el formato zip descargado en la siguiente ruta generada por xampp:
C:\xampp\htdocs

4.- Deberá crear una base de datos en postgres y en ella importar la base de datos que se le proporciona en el archivo "sgr.sql" dentro de la carpeta sistema_gestion_requerimientos\docs\bd del archivo zip descomprimido.

5.- Para poder configurar la base de datos, deberá modificar el archivo "conexion.php" que se encuentra dentro de la carpeta "SGR" y en ella, dentro de la carpeta "src". Deberá modificar, en la función "conectarPSQL()" la siguientes variables:

- $bd --> Aquí pondrá el nombre de la base de datos en la que ejecutó el código sql.

- $usuario --> Aquí pondrá un usuario postgres con permisos para insertar datos en tablas y eliminarlos con el que usted cuente, por ejemplo un superusuario.

- $password --> Aquí pondrá la contraseña de su usuario de la variable anterior. 

6.- Para acceder, deberá habilitar el servidor Apache dentro de Xampp. Abra el panel de control del software Xampp y de click en el botón "start" del renglón correspondinte a Apache. Cuando el nombre Apache esté en verde podrá ingresar al navegador.

7.- En el navegador tendrá que colocar la siguiente ruta como la URL:
localhost/sistema_gestion_requerimientos/SGR/scr/login.php

En dicha ruta se encutra el login.

8.- El usuario y contraseña del administrador son los siguientes:
Usuario: root@correo.mx
Contraseña: hola123.


